$(document).ready(function($){

    //fix menu
    $(window).bind('scroll', function() {
        // The value of where the "scoll" is.
        if($(window).scrollTop() > 50){
            $('nav').addClass('fixed');
        }else{
            $('nav').removeClass('fixed');
        }
    });
    //slider option

    var carouselContainer = $('#carousel-example-generic');
    var slideInterval = 5000;

    function toggleH(){
        $('.toggleHeading').hide();
        var caption = carouselContainer.find('.active').find('.toggleHeading').addClass('animated fadeInRight').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function (){
                $(this).removeClass('animated fadeInRight')});
        caption.slideToggle();
    }

    function toggleC(){
        $('.toggleCaption').hide();
        var caption = carouselContainer.find('.active').find('.toggleCaption').addClass('animated fadeInUp').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function (){
                $(this).removeClass('animated fadeInUp')
            });
        caption.slideToggle();
    }
    carouselContainer.carousel({
        interval: slideInterval, cycle: true, pause: "hover"})
        .on('slide.bs.carousel slid.bs.carousel', toggleH).trigger('slide.bs.carousel')
        .on('slide.bs.carousel slid.bs.carousel', toggleC).trigger('slide.bs.carousel');


    $('.packets-slide').slick({
        dots: false,
        autoplay: false,
        arrows: true,
        asNavFor: null,
        prevArrow: '<button type="button" data-role="none" class="slick-prev left"></button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next right"></button>',
        accessibility: true,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows: false,
                    dots: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows: false,
                    dots: true
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    //autoplay SPA slider

    $(".control-container button").click(function() {
        if ($(this).attr("id") === "pauseSpa") {
            $('#carousel-spa').carousel('pause');
            $(this).attr("id", "playSpa");
            $(this).toggleClass("play pause");
        } else {
            $('#carousel-spa').carousel('cycle');
            $(this).attr("id", "pauseSpa");
            $(this).toggleClass("pause play");
        }
    });

    //autoplay conferences slider

    $(".carousel-conferences button").click(function() {
        if ($(this).attr("id") === "pauseConferences") {
            $('#carousel-spa').carousel('pause');
            $(this).attr("id", "playConferences");
            $(this).toggleClass("play pause");
        } else {
            $('#carousel-spa').carousel('cycle');
            $(this).attr("id", "pauseConferences");
            $(this).toggleClass("pause play");
        }
    });

    // back to top
    $('.back-to-top').click(function() {      // When arrow is clicked
        $('body,html').animate({
            scrollTop : 0                       // Scroll to top of body
        }, 500);
    });

    // sidebar menu
    $('#navbar-collapse-1')
        .on('show.bs.collapse', function (e) {
            $('body').addClass('menu-slider');
        })
        .on('shown.bs.collapse', function (e) {
            $('body').addClass('in');
        })
        .on('hide.bs.collapse', function (e) {
            $('body').removeClass('menu-slider');
        })
        .on('hidden.bs.collapse', function (e) {
            $('body').removeClass('in');
        });

    // Customize map
    function initialise() {
        var myLatlng = new google.maps.LatLng(49.7205914, 19.041339); // Add the coordinates
        var mapOptions = {
            zoom: 10, // The initial zoom level when your map loads (0-20)
            zoomControl:true, // Set to true if using zoomControlOptions below, or false to remove all zoom controls.
            zoomControlOptions: {
                style:google.maps.ZoomControlStyle.DEFAULT // Change to SMALL to force just the + and - buttons.
            },
            center: myLatlng, // Centre the Map to our coordinates variable
            mapTypeId: google.maps.MapTypeId.ROADMAP, // Set the type of Map
            scrollwheel: false, // Disable Mouse Scroll zooming (Essential for responsive sites!)
            // All of the below are set to true by default, so simply remove if set to true:
            panControl:false, // Set to false to disable
            mapTypeControl:false, // Disable Map/Satellite switch
            scaleControl:false, // Set to false to hide scale
            streetViewControl:false, // Set to disable to hide street view
            overviewMapControl:false, // Set to false to remove overview control
            rotateControl:false // Set to false to disable rotate control
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions); // Render our map within the empty div
        var image = new google.maps.MarkerImage("img/maps-marker.svg", null, null, null, new google.maps.Size(30,38)); // Create a variable for our marker image.
        var marker = new google.maps.Marker({ // Set the marker
            position: myLatlng, // Position marker to coordinates
            icon:image, //use our image as the marker
            map: map, // assign the market to our map variable
            title: 'Kliknij aby zobaczyć więcej szczegółów' // Marker ALT Text
        });
        // 	google.maps.event.addListener(marker, 'click', function() { // Add a Click Listener to our marker
        //		window.location='https://www.snowdonrailway.co.uk/shop_and_cafe.php'; // URL to Link Marker to (i.e Google Places Listing)
        // 	});
        var infowindow = new google.maps.InfoWindow({ // Create a new InfoWindow
            content:"This is <strong>Hotel</strong>, <em>one</em> of shopping centres that has a cinema!" // HTML contents of the InfoWindow
        });
        google.maps.event.addListener(marker, 'click', function() { // Add a Click Listener to our marker
            infowindow.open(map,marker); // Open our InfoWindow
        });
        google.maps.event.addDomListener(window, 'resize', function() { map.setCenter(myLatlng); }); // Keeps the Pin Central when resizing the browser on responsive sites
    }
    google.maps.event.addDomListener(window, 'load', initialise); // Execute our 'initialise' function once the page has loaded.
});